======
README
======


Notes:
======
- Plugin Command-T requires vim compiled with Ruby
- Plugin Powerline requires vim compiled with --with-features=big
- Plugin Syntastic is set to use flake8 to check python files. Install it using pip.

Install steps:
==============
- Pull all the submodules down first::

    >> git submodule init
    >> git submodule update --recursive

- Let vim read the settings::

    >> ln -sf <PROJECT_DIR>/vimrc ~/.vimrc
    >> ln -sf <PROJECT_DIR>/vim ~/.vim

Please follow vim/Command-T/README.txt to install Command-T, basic steps as follows::

    >> cd vim/bundle/Command-T
    >> git submodule init
    >> git submodule update
    >> make
    >> cd ruby/command-t
    >> export ARCHFLAGS="-arch <ARCH>"  (find it using 'vim --version | grep arch')
    >> ruby extconf.rb
    >> make
    >> sudo make install
