"use ':so %' to reload this vim settings on the fly

" Enable pathogen plugin
let g:pathogen_disabled = []
if $DISABLE_COMMAND_T == '1'
    call add(g:pathogen_disabled, 'Command-T')
endif

runtime bundle/vim-pathogen/autoload/pathogen.vim
call pathogen#infect()
call pathogen#helptags()

" General options
syntax enable
syntax on
colorscheme koehler
filetype plugin on
filetype indent on

set nu!
set ruler
set laststatus=2
set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'
set hlsearch
set foldmethod=marker
set nocompatible
set shellslash

set smarttab
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set cindent

set mouse=a
set guifont=Monaco:h14
set guioptions-=T
set antialias

set backspace=indent,eol,start
set encoding=utf-8

let mapleader="`"

set vb t_vb=
set guioptions-=T

set guifont=Monaco:h14
set antialias

map <C-h> :tabprevious<cr>
map <C-l> :tabnext<cr>

" Map *.as to actionscript
au BufRead,BufNewFile *.as set filetype=actionscript

" Powerline settings
let g:Powerline_symbols = 'fancy'

let g:disable_pydiction_plugin=1

" Nerdtree settings
map <leader>n :NERDTreeToggle<cr>
map <leader>m :TlistToggle<cr>

" Automatically remove the trailing spaces when saving a file
autocmd BufWritePre * :%s/\s\+$//e

autocmd filetype python call PythonInit()
autocmd filetype java call JavaInit()
autocmd filetype ruby call RubyEnvInit()
autocmd filetype yaml call YamlEnvInit()
autocmd filetype ps1 call PowershellInit()
autocmd filetype actionscript call ActionScriptInit()
autocmd filetype html call HTMLInit()
autocmd filetype javascript call JSEnvInit()

let tlist_objc_settings = 'ObjectiveC;P:protocols;i:interfaces;types(...)'

autocmd BufWritePre * :%s/\s\+$//ge

function! PythonInit()
    " Use flake8 as the python checker. Require flake8 installed.
    " It can be installe using pip
    let g:syntastic_python_syntax_checker = ['flake8']
    " Override the max line length warning value, which is 79 before
    let g:syntastic_python_checker_args = '--max-line-length=140'

    let g:disable_pydiction_plugin=0
    let g:pydiction_location='~/.vim/bundle/Pydiction/complete-dict'
endfunction

function! JavaInit()
    let g:syntastic_java_checker = 'checkstyle'
    let g:syntastic_java_checkstyle_classpath = '~/algs4/checkstyle-5.5/checkstyle-5.5-all.jar'
    let g:syntastic_java_checkstyle_conf_file = '~/algs4/checkstyle-5.5/checkstyle.xml'
endfunction

function! PowershellInit()
    set iskeyword-=-
endfunction

function! ActionScriptInit()
    let tlist_actionscript_settings = 'actionscript;c:class;f:method;p:property;v:variable'
endfunction

function! YamlEnvInit()
    set tabstop=2
    set shiftwidth=2
    set softtabstop=2
endfunction

function! RubyEnvInit()
    set tabstop=2
    set shiftwidth=2
    set softtabstop=2

    let g:rspec_runner = "os_x_iterm"
endfunction

function! HTMLInit()
    le g:syntastic_html_tidy_ignore_errors=["trimming empty"]
endfunction

function! JSEnvInit()
    set tabstop=2
    set shiftwidth=2
    set softtabstop=2
endfunction
